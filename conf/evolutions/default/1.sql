# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table access_tokens (
  id                        bigint auto_increment not null,
  token                     varchar(255) not null,
  user_id                   bigint not null,
  ttl                       integer not null,
  created_at                datetime not null,
  updated_at                datetime not null,
  constraint pk_access_tokens primary key (id))
;

create table assigneds (
  id                        bigint auto_increment not null,
  subject_id                bigint not null,
  schedule_id               bigint not null,
  created_at                datetime not null,
  updated_at                datetime not null,
  constraint pk_assigneds primary key (id))
;

create table days (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  start_time                varchar(255),
  end_time                  varchar(255),
  constraint pk_days primary key (id))
;

create table schedules (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  user_id                   bigint not null,
  created_at                datetime not null,
  updated_at                datetime not null,
  constraint pk_schedules primary key (id))
;

create table subjects (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  section                   integer not null,
  created_at                datetime not null,
  updated_at                datetime not null,
  constraint pk_subjects primary key (id))
;

create table to_dos (
  id                        bigint auto_increment not null,
  message                   varchar(1000) not null,
  due_date                  datetime,
  should_notify             tinyint(1) default 0 not null,
  is_home_work              tinyint(1) default 0 not null,
  assigned_id               bigint,
  created_at                datetime not null,
  updated_at                datetime not null,
  constraint pk_to_dos primary key (id))
;

create table users (
  id                        bigint auto_increment not null,
  username                  varchar(255) not null,
  password                  varchar(255) not null,
  email                     varchar(255) not null,
  created_at                datetime not null,
  updated_at                datetime not null,
  constraint pk_users primary key (id))
;


create table assigneds_days (
  assigneds_id                   bigint not null,
  days_id                        bigint not null,
  constraint pk_assigneds_days primary key (assigneds_id, days_id))
;
alter table access_tokens add constraint fk_access_tokens_user_1 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_access_tokens_user_1 on access_tokens (user_id);
alter table assigneds add constraint fk_assigneds_subject_2 foreign key (subject_id) references subjects (id) on delete restrict on update restrict;
create index ix_assigneds_subject_2 on assigneds (subject_id);
alter table assigneds add constraint fk_assigneds_schedule_3 foreign key (schedule_id) references schedules (id) on delete restrict on update restrict;
create index ix_assigneds_schedule_3 on assigneds (schedule_id);
alter table schedules add constraint fk_schedules_user_4 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_schedules_user_4 on schedules (user_id);
alter table to_dos add constraint fk_to_dos_assigned_5 foreign key (assigned_id) references assigneds (id) on delete restrict on update restrict;
create index ix_to_dos_assigned_5 on to_dos (assigned_id);



alter table assigneds_days add constraint fk_assigneds_days_assigneds_01 foreign key (assigneds_id) references assigneds (id) on delete restrict on update restrict;

alter table assigneds_days add constraint fk_assigneds_days_days_02 foreign key (days_id) references days (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table access_tokens;

drop table assigneds;

drop table assigneds_days;

drop table days;

drop table schedules;

drop table subjects;

drop table to_dos;

drop table users;

SET FOREIGN_KEY_CHECKS=1;

