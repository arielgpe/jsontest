name := """jsontest"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  "org.mariadb.jdbc" % "mariadb-java-client" % "1.1.7",
  "org.mindrot" % "jbcrypt" % "0.3m",
  javaJdbc,
  javaEbean,
  cache,
  javaWs
)
