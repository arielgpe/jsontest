package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Subjects;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 12/6/14
 */

public class Subject extends SecuredController {

    public static Result get_subjects() {
        List<Subjects> subjectsList = Subjects.findAll();
        if (subjectsList.isEmpty()){
            return noContent();
        } else {
            return ok(Json.toJson(subjectsList));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result post_subjects() {
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").textValue();
        int section = json.findPath("section").intValue();
        if(name == null) {
            return badRequest("Missing parameter [name]");
        } else {
            Subjects subject = Subjects.create(name, section);
            return ok(Json.toJson(subject));
        }
    }
}
