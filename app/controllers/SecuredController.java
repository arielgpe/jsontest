package controllers;

import play.mvc.Controller;
import play.mvc.With;

/**
 * Created by Ariel Guzman on 12/27/14
 */
@With(SecuredAction.class)
public abstract class SecuredController extends Controller {
}
