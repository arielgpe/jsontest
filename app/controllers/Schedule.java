package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Schedules;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 12/7/14
 */
public class Schedule extends SecuredController {

    public static Result get_schedules() {
        List<Schedules> schedulesList = Schedules.findAll();
        return ok(Json.toJson(schedulesList));
    }

    public static Result get_schedule(Long id) {
        Schedules schedule = Schedules.findById(id);
        return ok(Json.toJson(schedule));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result post_schedules() {
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").textValue();
        if(name == null) {
            return badRequest("Missing parameter [name]");
        } else {
            Schedules schedule = Schedules.create(name);
            return ok(Json.toJson(schedule));
        }
    }
}
