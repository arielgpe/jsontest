package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.AccessTokens;
import models.Users;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by Ariel Guzman on 12/25/14
 */
public class User extends Controller {

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create(){
        JsonNode json = request().body().asJson();
        String username = json.findPath("username").textValue();
        String password = json.findPath("password").textValue();
        String email = json.findPath("email").textValue();
        if(username == null || password == null || email == null) {
            return badRequest("Missing parameter");
        } else {
            Users user = Users.create(username, password, email);
            if (user == null) {
                ObjectNode result = Json.newObject();
                result.put("error", "Username or Email already exists");
                return unauthorized(result);
            } else {
                return ok(Json.toJson(user));
            }
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result login(){
        JsonNode json = request().body().asJson();
        String username = json.findPath("username").textValue();
        String password = json.findPath("password").textValue();
        if(username == null || password == null) {
            return badRequest("Missing parameter");
        } else {
            AccessTokens user = Users.login(username, password);
            if (user == null) {
                return unauthorized("Wrong username or password");
            } else {
                return ok(Json.toJson(user));
            }
        }
    }

    public static Result logout(){
        int resultCode = Users.logout();
        ObjectNode result = Json.newObject();
        if (resultCode == 0) {
            return ok();
        } else if (resultCode == 1){
            return internalServerError();
        } else {
            result.put("Error", "Authorization header not found");
            return badRequest(result);
        }
    }
}
