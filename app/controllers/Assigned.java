package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Assigneds;
import models.Schedules;
import models.Subjects;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 12/7/14
 */
public class Assigned extends SecuredController {

    public static Result get_assigneds() {
        List<Assigneds> assignedsList = Assigneds.findAll();
        return ok(Json.toJson(assignedsList));
    }

    public static Result get_assiged(Long id){
        Assigneds assigned = Assigneds.findById(id);
        return ok(Json.toJson(assigned));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result post_assigneds(){
        JsonNode json = request().body().asJson();
        Long subject = json.findPath("subject_id").longValue();
        Long schedule = json.findPath("schedule_id").longValue();
        if ((subject == 0) || (schedule == 0)) {
            return badRequest("Missing parameters");
        } else {
            Assigneds assigned = Assigneds.create(Subjects.findById(subject),
                    Schedules.findById(schedule));
            return ok(Json.toJson(assigned));
        }
    }
}
