package controllers;

import models.AccessTokens;
import play.Logger;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

/**
 * Created by Ariel Guzman on 12/27/14
 */
public class SecuredAction extends Action.Simple {
    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {
        String token = getTokenFromHeaders(context);
        if (token != null) {
            Logger.debug(token);
            AccessTokens user = AccessTokens.findOne(token);
            if (user != null){
                context.request().setUsername(user.user.username);
                return delegate.call(context);
            }
        }
        Result unauthorized = Results.unauthorized("unauthorized");
        return F.Promise.pure(unauthorized);
    }

    public static String getTokenFromHeaders(Http.Context context){
        String[] authTokenHeaderValues = context.request().headers().get("Authorization");
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            return authTokenHeaderValues[0];
        }
        return null;
    }
}
