package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 12/7/14
 */
@Entity
public class Assigneds extends Model {

    @Id
    public Long id;

    @OneToOne
    @NotNull
    public Subjects subject;

    @ManyToOne
    @NotNull
    @JsonBackReference
    public Schedules schedule;

    @OneToMany
    @JsonManagedReference
    List<ToDos> toDosList = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    public List<Days> days = new ArrayList<>();

    @CreatedTimestamp
    public Date createdAt;

    @UpdatedTimestamp
    public Date updatedAt;

    //Custom Methods

    private static Finder<Long, Assigneds> find(){
        return new Finder<>(Long.class, Assigneds.class);
    }

    public static List<Assigneds> findAll(){
        return find().all();
    }

    public static Assigneds findById(Long id){
        return find().byId(id);
    }

    public static Assigneds create(Subjects subject, Schedules schedule){
        Assigneds assigned = new Assigneds();
        assigned.subject = subject;
        assigned.schedule = schedule;
        assigned.save();
        return assigned;
    }
}
