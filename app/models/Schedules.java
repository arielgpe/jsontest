package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import controllers.SecuredAction;
import play.db.ebean.Model;
import play.mvc.Http;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 12/6/14
 */
@Entity
public class Schedules extends Model {

    @Id
    public Long id;

    @NotNull
    public String name;

    @OneToMany(mappedBy = "schedule", cascade = CascadeType.ALL)
    @JsonManagedReference
    public List<Assigneds> assigneds = new ArrayList<>();

    @ManyToOne
    @NotNull
    @JsonIgnore
    public Users user;

    @CreatedTimestamp
    public Date createdAt;

    @UpdatedTimestamp
    public Date updatedAt;

    private static Finder<Long, Schedules> find() {
        return new Finder<>(Long.class, Schedules.class);
    }

    public static List<Schedules> findAll() {
       Http.Context context = Http.Context.current();
       String token = SecuredAction.getTokenFromHeaders(context);
        return find().where().eq("user_id", AccessTokens.findOne(token).user.id).findList();
    }

    public static Schedules findById(Long id){
        return find().byId(id);
    }

    public static Schedules create(String name){
        Schedules schedule = new Schedules();
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        schedule.name = name;
        schedule.user = AccessTokens.findOne(token).user;
        schedule.save();
        return schedule;
    }

}
