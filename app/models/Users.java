package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import controllers.SecuredAction;
import org.mindrot.jbcrypt.BCrypt;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.Http;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Ariel Guzman on 12/25/14
 */
@Entity
public class Users extends Model {
    @Id
    public Long id;

    @NotNull
    public String username;

    @NotNull
    @JsonIgnore
    public String password;

    @NotNull
    @Constraints.Email
    public String email;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonBackReference
    public List<AccessTokens> accessTokens = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonBackReference
    public List<Schedules> schedules = new ArrayList<>();

    @CreatedTimestamp
    public Date createdAt;

    @UpdatedTimestamp
    public Date updatedAt;

    private static Finder<Long, Users> find(){
        return new Finder<>(Long.class, Users.class);
    }

    public static Users create(String username, String password, String email){
        Users name = find().where().eq("username", username).findUnique();
        Users mail = find().where().eq("email", email).findUnique();
        if ((name != null) || (mail != null)){
            return null;
        }  else {
            Users user = new Users();
            user.username = username;
            user.password = BCrypt.hashpw(password, BCrypt.gensalt());
            user.email = email;
            user.save();

            return user;
        }
    }

    public static AccessTokens login(String username, String password){
        Users user = find().where().eq("username", username).findUnique();
        if(user != null && BCrypt.checkpw(password, user.password)){
            AccessTokens accessToken = new AccessTokens();
            accessToken.user = user;
            accessToken.token = UUID.randomUUID().toString();
            accessToken.save();
            return accessToken;
        } else {
            return null;
        }
    }

    public static int logout(){
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        if (token != null) {
            AccessTokens accessTokens = AccessTokens.findOne(token);
            if (accessTokens != null) {
                accessTokens.delete();
                return 0;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
    }
}
