package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Created by Ariel Guzman on 12/7/14
 */
@Entity
public class Days extends Model {
    @Id
    public long id;

    @NotNull
    public String name;

    public String startTime;

    public String endTime;
}
