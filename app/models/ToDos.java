package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Ariel Guzman on 12/7/14
 */
@Entity
public class ToDos extends Model {
    @Id
    public Long id;

    @Column(length = 1000)
    @NotNull
    public String message;

    public Date dueDate;

    @NotNull
    public Boolean shouldNotify;

    @NotNull
    public Boolean isHomeWork;

    @ManyToOne
    public Assigneds assigned;

    @CreatedTimestamp
    public Date createdAt;

    @UpdatedTimestamp
    public Date updatedAt;
}
