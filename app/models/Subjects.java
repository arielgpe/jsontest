package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 12/6/14
 */
@Entity
public class Subjects extends Model {

    @Id
    public Long id;

    @NotNull
    public String name;

    @NotNull
    public int section;

    @CreatedTimestamp
    public Date createdAt;

    @UpdatedTimestamp
    public Date updatedAt;

    private static Finder<Long, Subjects> find () {
        return new Finder<>(Long.class, Subjects.class);
    }

    public static List<Subjects> findAll() {
        return find().all();
    }

    public static Subjects findById(Long id){
        return find().byId(id);
    }

    public static Subjects create(String name, int section){
        Subjects subject = new Subjects();
        subject.name = name;
        subject.section = section;
        subject.save();
        return subject;
    }
}
