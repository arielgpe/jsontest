/**
 * Created by arielgpe on 12/27/14.
 */
angular.module("scheduleApp", [
    'scheduleApp.controllers',
    'apiService',
    'ngMaterial',
    'ngStorage',
    'ui.router'
])
    .config(function($stateProvider, $urlRouterProvider){
        $stateProvider
            .state('auth',{
                url: '/auth',
                abstract: true,
                templateUrl: '/assets/templates/auth/index.html'
            })
            .state('auth.login',{
                url: '/login',
                templateUrl: '/assets/templates/auth/login.html'
            })


            .state('app',{
                abstract: true,
                templateUrl: '/assets/templates/app/index.html'
            })
            .state('app.dashboard',{
                url: '',
                templateUrl: '/assets/templates/app/dashboard.html'
            });

        $urlRouterProvider.otherwise("");
    });