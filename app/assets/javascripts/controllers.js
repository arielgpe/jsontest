/**
 * Created by arielgpe on 12/27/14.
 */
angular.module('scheduleApp.controllers', [])

    .controller('AppController', function($scope, User, $mdToast){
        $scope.user = {
            username: '',
            password: '',
            loading: false
        };
        $scope.login = function(){
            $scope.user.loading = true;
            User.login($scope.user, function(success){
                $scope.user.loading = false;
                console.log(success);
            },function(error){
                $scope.user.loading = false;
                console.log(error);
                $mdToast.show(
                    $mdToast.simple()
                        .content(error.data)
                        .position("top right")
                        .hideDelay(5000)
                );
            });
        };
    });