/**
 * Created by arielgpe on 12/27/14.
 */
angular.module('apiService', ['ngResource'])
    .constant('BASE_URL', 'http://10.0.0.5:9000/api/v1/')
    .factory('User', function(BASE_URL, $resource, $localStorage){
        return $resource(null, null, {
            login: {
                url: BASE_URL + 'users/login',
                method: 'POST',
                interceptor: {
                    response: function(response){
                        $localStorage.user = response.data.user;
                        $localStorage.userId = response.data.user.id;
                        $localStorage.token = response.data.token;
                        return response.resource;
                    }
                }
            }
        });
    });